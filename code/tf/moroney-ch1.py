#!/usr/bin/env python3

# From https://learning.oreilly.com/library/view/ai-and-machine/9781492078180/ch01.html#using_tensorflow_in_google_colab 

import tensorflow as tf
import numpy as np
import os,sys
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense

xs = np.array([-1.0, 0.0, 1.0, 2.0, 3.0, 4.0], dtype=float)
ys = np.array([-3.0, -1.0, 1.0, 3.0, 5.0, 7.0], dtype=float)

if __name__ == "__main__":
  if len(sys.argv) > 3:

    if len(sys.argv) > 4:
      my_optimizer = sys.argv[5]
    else:
      my_optimizer = 'sgd'

    model = Sequential([Dense(units=int(sys.argv[1]), input_shape=[int(sys.argv[2])])])
    model.compile(optimizer=my_optimizer, loss='mean_squared_error')
    model.fit(xs, ys, epochs=int(sys.argv[3]))
    print(model.predict([float(sys.argv[4])]))

  else:
    print("Foo <units> <input_shape> <epoch> <predict> <optimizer>")
    print("\twhere optimizer can be adam, sgd, adamx")

"""
Chat GPT summary of the code 

1. Creating the model:
   - The `Sequential` class from `tensorflow.keras` is used to create a sequential model. This means that the layers are stacked linearly, one after another.
   - Inside the `Sequential` constructor, we pass a list containing a single layer: `Dense(units=1, input_shape=[1])`.
   - The `Dense` layer represents a fully connected layer, where all the neurons are connected to the previous and next layers. It has 1 unit, indicating the number of neurons, and an input shape of `[1]`, specifying that the input data has one dimension.

2. Compiling the model:
   - The `compile` method is called on the model to configure the learning process.
   - In this code, we set the optimizer as `'sgd'`, which stands for stochastic gradient descent. It is a popular optimization algorithm used to update the model's parameters during training.
   - The loss function is set to `'mean_squared_error'`, which calculates the mean squared difference between the predicted and actual values. The goal is to minimize this loss during training.

3. Training the model:
   - The `fit` method is used to train the model on the given input and output data.
   - `xs` and `ys` are NumPy arrays containing the input and output data, respectively.
   - The `epochs` parameter specifies the number of times the model will iterate over the entire dataset during training.
   - During each epoch, the model adjusts its internal parameters to minimize the loss between predicted and actual outputs.

4. Making predictions:
   - Finally, the `predict` method is called on the model to obtain predictions for new input data.
   - In this code, `[10.0]` is passed as the input to predict the corresponding output value.
   - The model uses its learned parameters to compute the predicted output.

Overall, this code uses TensorFlow's high-level APIs to create, compile, train, and make predictions with a simple neural network model for linear regression. It demonstrates the typical workflow for training a model using TensorFlow.

"""
