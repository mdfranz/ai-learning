#!/usr/bin/env python3


import os,cohere,time
import numpy as np

"""
  See 
    https://numpy.org/doc/stable/reference/routines.linalg.html
    https://numpy.org/doc/stable/reference/generated/numpy.linalg.norm.html
"""

def calculate_similarity(a, b):
  return np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))


if __name__ == "__main__":
  co = cohere.Client(os.environ["COHERE_API_KEY"])

  print(co.detect_language(texts=["Hola"]).results)
  print(co.detect_language(texts=["¡Hola"]).results)
  print(co.detect_language(texts=["Saludos"]).results)
  print(co.detect_language(texts=["Hallo"]).results)
  print(co.detect_language(texts=["Hello"]).results)
  print(co.detect_language(texts=["Howdy"]).results)
  print(co.detect_language(texts=["Bonjour"]).results)

  for m in ['embed-english-light-v2.0','embed-english-v2.0','embed-multilingual-v2.0']:
    e1 = co.embed(texts=['hello','goodbye'],model=m).embeddings
    e2 = co.embed(texts=['hi','bye'],model=m).embeddings
    e3 = co.embed(texts=['hi','howdy'],model=m).embeddings
    e4 = co.embed(texts=['hi','hallo'],model=m).embeddings
    #e3 = co.embed(texts=['hello','hi'],model=m).embeddings
    #e4 = co.embed(texts=['hello','bye'],model=m).embeddings

    print (f'\n------ Model size: {m}')
    print ("Hello and Hi")
    print (calculate_similarity(e1[0],e2[0]))

    print ("Hi and Howdy")
    print (calculate_similarity(e3[0],e3[1]))

    print ("Hello and Bye")
    print (calculate_similarity(e1[0],e2[1]))

    print ("Hello and GoodBye")
    print (calculate_similarity(e1[0],e1[1]))

    print ("Bye and GoodBye")
    print (calculate_similarity(e2[1],e1[1]))

    print ("Hi and Hallo")
    print (calculate_similarity(e4[0],e4[1]))


    #print ("\n=== Second Try ====")
    #print ("Hello and Hi")
    #print (calculate_similarity(e3[0],e3[1]))

    #print ("Hello and Bye")
    #print (calculate_similarity(e4[0],e4[1]))

    time.sleep(10)

  #print (e2)

