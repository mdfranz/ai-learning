#!/usr/bin/env python3

# Assumes COHERE_API_KEY is set

import cohere, os, time, sys

co = cohere.Client(os.environ['COHERE_API_KEY'])

text = """The founder of the far-right Oath Keepers has been sentenced to 18 years in federal prison in connection with the Jan. 6 attack on the Capitol following his conviction on seditious conspiracy. The sentence for Stewart Rhodes is the longest imposed on a Jan. 6 defendant to date. In a politically-charged speech in the courtroom just before his sentencing, he called himself a "political prisoner" and said that when he talked about "regime change" in a phone call with supporters earlier this week, he meant he hopes that former President Donald Trump will win in 2024.
"""

if len(sys.argv) < 2:
  print("Usage summarize.py <model> <length> <extractiveness> <temperature>")
else:
  r = co.summarize(
    text=text,
    model=sys.argv[1],
    length=sys.argv[2],
    extractiveness=sys.argv[3],
    temperature=int(sys.argv[4])
  )

  print(r.summary)
  print()

  
