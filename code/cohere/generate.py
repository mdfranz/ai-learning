#!/usr/bin/env python3

import os,cohere,time

if __name__ == "__main__":
  co = cohere.Client(os.environ["COHERE_API_KEY"])

  for m in ['command-light','command']:
    for mt in (20,50,100):
      for t in (.25,.5, .75, .1):

        print (f'Model: {m} Max Tokens {mt} Temporature {t}')
        print (co.generate(prompt="This morning I woke up and",model=m,max_tokens=mt,temperature=t))
        time.sleep(20)
