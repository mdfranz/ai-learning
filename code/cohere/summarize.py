#!/usr/bin/env python3

import os,cohere,time

if __name__ == "__main__":
  co = cohere.Client(os.environ["COHERE_API_KEY"])

  txt = """
  If past is prologue, then the 2011 debt ceiling debate informed the 2023 debt ceiling drama Congress faced this week.  In 2011, Republican Speaker John Boehner tried to reconcile debt ceiling politics in a divided government. Back then, the Republicans controlled the House, Democrats the Senate and Barack Obama the White House. The tea party wave of 2010 delivered Republicans a substantial House majority, and with that rambunctious majority came unrealistic expectations of what could be accomplished in divided government. To mollify Republican hard-liners, the GOP House leadership team of Boehner, Majority Leader Eric Cantor and Majority Whip Kevin McCarthy advanced legislation dubbed “cut, cap and balance,” an aggressive measure to increase the debt limit, cut federal spending, cap future spending and allow a constitutional amendment to balance the federal budget.  This time, McCarthy, R-Calif., prevailed in impressive fashion with a strong debt ceiling vote among his House GOP colleagues, reinforcing his position as speaker. Expect McCarthy’s harshest critics to plot their next move. A motion to vacate the chair is the nuclear option for the hard-liners. But do not expect such a kamikaze move now, as it would infuriate the vast majority of the House GOP Conference. Fortunately for McCarthy, his angry detractors are not strategic thinkers, and they are likely to misfire in whatever they do.  """

  print (txt)

  for m in ['summarize-medium','summarize-xlarge']:
    for l in ("short","long"):
      for t in (.3,.6, .9):
        for e in ("auto","low","high"):

          print (f'\nModel: {m} Temp {t} Size {l} Extractiveness {e}')
          print (co.summarize(text=txt, model=m, length=l, temperature=t,extractiveness=e).summary)
          time.sleep(15)
