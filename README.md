# ai-learning
The goal of this document is to capture the state of my knowledge and understanding from different vantage points. An interesting view of AI is through [capabilities, interfaces, frameworks, and applications](https://glazkov.com/2023/05/29/four-layers/) 

Also see https://gitlab.com/mdfranz/cheetsheetz/-/tree/master/ai for links.

## Fundamentals and Foundations
### Python
For any Deep Learning, you should understand the basics of [numpy](https://numpy.org/doc/stable/) such as [ndarrays](https://numpy.org/doc/stable/reference/arrays.ndarray.html) in particular [indexing and slicing](https://numpy.org/doc/stable/user/basics.indexing.html) given this is needed for vectors and embeddings. Working with tabular data requires a good understanding [Series](https://pandas.pydata.org/docs/reference/api/pandas.Series.html) and [DataFrame](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) and [viewing data with loc/iloc](https://towardsdatascience.com/how-to-use-loc-and-iloc-for-selecting-data-in-pandas-bd09cb4c3d79) and [filtering row data](https://towardsdatascience.com/different-ways-to-filter-pandas-dataframe-1899eb6d4be8)

Most examples assume you know how to work with [IPython](https://ipython.readthedocs.io/en/stable/interactive/tutorial.html), [Jupyter](https://docs.jupyter.org/en/latest/projects/content-projects.html) and the various hosted notebooks such as [SageMaker](https://docs.aws.amazon.com/sagemaker/latest/dg/nbi.html) and [Colab](https://colab.research.google.com/)

## Companies

### AI21 Labs ###
[AI21 Labs](https://www.ai21.com/) is an Israli startup that provides the [Jurassic-2](https://docs.ai21.com/docs/jurassic-2-models) Models with a focus similar to Cohere and you can easily sign up for a free version with Google/Microsoft SSO.

### HuggingFace
A [platform](https://huggingface.co/docs/hub/index) for building AI/ML apps, models, and datasets. The key abstractions are [repositories](https://huggingface.co/docs/hub/repositories), [models](https://huggingface.co/docs/hub/models), and [datasets](https://huggingface.co/docs/hub/datasets-overview) as well as [spaces](https://huggingface.co/spaces) which are a way to host apps. A base level of features are available for [free](https://huggingface.co/pricing) with a Pro Account for $9/month.

### OpenAI
Provides API Access and Free/Upgrade Chat access to their Language Models for chat, text, image, and audio. Text models include davinci, babbage, curie, and a variety of code- models that can be accessed via [LangChain](https://python.langchain.com/en/latest/modules/models/llms/integrations/openai.html)  with [usage-based pricing](https://openai.com/pricing). OpenAI is billed seperately from [ChatGPT Plus](https://chat.openai.com/) which $20/month. 

### Cohere ###


### Pinecone
A SaaS [Vector Database](https://gitlab.com/mdfranz/cheetsheetz/-/blob/master/ai/vectordb.md)  that can be used to host vector embeddings and perform vector searches within an [index](https://docs.pinecone.io/docs/indexes) across pods. Vector indexes can be created with `dotproduct`, `cosine` distance, and `euclidean` metrics to perform [similiarity searches](https://www.pinecone.io/learn/what-is-similarity-search/) and other operations. 

Allows [free starter](https://www.pinecone.io/pricing/) accounts and support SSO with Google and GitHub. 

## Tools & Frameworks

### LangChain

## Backend Components

### Vector Databases

#### Milvus

[Milvus](https://milvus.io/) seems to have have the most features, but a more complex architecture, although it does include [a light version](https://github.com/milvus-io/milvus-lite) that can be embedded in Python similar to Polars. 

### Large Language Model

#### ChatGPT ####

## Concepts
### Transformers
